import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import services from '../URLs/services';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

function BusinessTool() {
    const [env, setEnv] = useState("qa2")
    const [allServices, setAllServices] = useState(services[env])
    const copyToClipboard = (q) => {
        navigator.clipboard.writeText(q)
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }

    const confirmAndSet = () => {
        const pass = localStorage.getItem("prodPassword") || prompt("Enter the password")
        if (pass !== "cCE,}jY+7z<p") {
            return alert("Wrong password")
        }
        else {
            localStorage.setItem("prodPassword", pass)
        }
        setEnv("prod")
    }

    useEffect(() => {
        setAllServices(services[env])
    }, [env])

    return (
        <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
            <div className="d-flex p-2">
                <UncontrolledDropdown>
                    <DropdownToggle caret>
                        &nbsp;Business Tool -  {env} &nbsp;
                    </DropdownToggle>
                    <DropdownMenu >
                        <DropdownItem onClick={() => setEnv("qa2")}>QA2</DropdownItem>
                        <DropdownItem onClick={() => setEnv("preprod")}>Preprod</DropdownItem>
                        <DropdownItem onClick={confirmAndSet}>Prod</DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </div>
            <div style={{ flex: 1, overflow: 'auto' }}>
                <iframe style={{ border: 'none', height: '120%', width: '120%',transformOrigin:'left top', transform: 'scale(.8)' }} src={allServices.business_tool.url}></iframe>
            </div>
        </div >
    )
}

export default BusinessTool