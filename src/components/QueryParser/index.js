import React, { useEffect, useState } from 'react'
import ReactJson from 'react-json-view'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import { qpsAPI } from '../../api/qps';
import { mappingsAPI } from '../../api/mappings';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

function QueryParser() {
    const [searchTerm, setSearchTerm] = useState('')
    const [categories, setCategories] = useState([])
    const [version, setVersion] = useState('v3')
    const [response, setResponse] = useState([])
    const copyToClipboard = () => {
        localStorage.setItem("searchTerm", searchTerm)
        const pipeSeparated = categories.map(category => category[0]).join("|")
        localStorage.setItem("categories", pipeSeparated)
        navigator.clipboard.writeText(pipeSeparated)
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }

    const getData = () => {
        qpsAPI.getQpsData(searchTerm, version).then(e => {
            console.log(e)
            setResponse(e.data)
            const cats = e.data.filters?.find(e => e?.field_name == "category_en_string_mv").values
            if (cats)
                mappingsAPI.getDescriptions(cats).then(d => setCategories(d.data))
        })
    }

    const handleSubmit = async e => {
        e.preventDefault()
        getData()
    }
    return (
        <div>
            <Box display="flex" alignItems="center">
                <Box display="flex" alignItems="center">
                    <div className="d-flex p-2">
                        <UncontrolledDropdown>
                            <DropdownToggle caret>
                                &nbsp;Version - {version} &nbsp;
                            </DropdownToggle>
                            <DropdownMenu >
                                <DropdownItem onClick={() => setVersion("v1")}>v1</DropdownItem>
                                <DropdownItem onClick={() => setVersion("v2")}>v2</DropdownItem>
                                <DropdownItem onClick={() => setVersion("v3")}>v3</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </Box>
                <Form style={{ display: 'flex', flex: 1 }} className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                    <Input style={{ width: '50%' }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Search queries" className='focus' />
                    <Button onClick={handleSubmit}>Find</Button>
                    <Button onClick={copyToClipboard}>Copy</Button>
                </Form>
            </Box>
            {/* {responses.length > 0 && <Box mt={2}>
                <Table responsive>
                    <thead>
                        <tr>
                            <th className="w-25">Original</th>
                            <th className="w-25">Corrected</th>
                            <th className="w-25">Normalized</th>
                        </tr>
                    </thead>
                    <tbody>
                        {[...responses, ...responses, ...responses, ...responses].map(response => <tr>
                            <td className="w-25">{response.query}</td>
                            <td className="w-25">{response.corrected_query}</td>
                            <td className="w-25">{response.normalized_query} <i class="fa fa-solid fa-copy text-primary" style={{ fontSize: 18, cursor: 'pointer' }} onClick={copyToClipboard}></i></td>
                        </tr>)}
                    </tbody>
                </Table>
            </Box>} */}
            {/* <Table>
                <tbody>
                    {categories.map(category => {
                        return <tr>
                            <td>{category[0]}</td>
                            <td>{category[1]}</td>
                        </tr>
                    })}
                </tbody>
            </Table> */}
            <Box>

                {/* Object.keys(response).map(e => <pre style={{ color: 'white', background: "black", margin: 10 }} dangerouslySetInnerHTML={{ __html: JSON.stringify({ [e]: response[e] }, null, 2) }}></pre>) */}
                <ReactJson theme="monokai" displayDataTypes={false} src={response} />

            </Box>
        </div >
    )
}

export default QueryParser