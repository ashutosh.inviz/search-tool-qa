import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box, Typography } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import { mappingsAPI } from '../../api/mappings';
import { facetsAPI } from '../../api/facets';
import { rankingAPI } from '../../api/ranking';
import { elasticAPI } from '../../api/elastic';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

const heading = {
    qa2: 'Service',
    core: 'Core',
    bff: 'BFF'
}

const APIs = [
    { api: "getServiceRanking", name: 'qa2' }, { api: 'getBFFRanking', name: 'bff' }
]

function Elasticsearch() {
    const [searchTerm, setSearchTerm] = useState('')
    const [response, setResponse] = useState()
    const [index, setIndex] = useState('tata_luxury_tata_qa2')
    const [productId, setProductId] = useState('')
    const [category, setCategory] = useState('')
    const [limit, setLimit] = useState()
    const [spellCheckResponse, setSpellCheckResponse] = useState({})
    const [loading, setLoading] = useState(false)
    const copyToClipboard = (env) => {
        navigator.clipboard.writeText(response[env].map(pr => pr.product_id.toLowerCase()).join(" "))
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }

    const handleSubmit = async e => {
        const url = elasticAPI.getProductURL(index, "MP000000009123416")
        console.log(url)
        window.location.href = url
    }

    return (
        <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
            <Box display="flex">
                <Form style={{ flex: 1 }} className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                    <Input style={{ width: '100%', marginRight: 10 }} onChange={e => setProductId(e.target.value)} value={productId} id="search-input" placeholder="Product Id" className='focus' />
                </Form>
                <Form style={{ flex: 1 }} className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                    <Input style={{ width: '100%', marginRight: 10 }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Search term" className='focus' />
                </Form>
                <Form style={{ flex: 1 }} className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                    <Input style={{ width: '100%', marginRight: 10 }} onChange={e => setCategory(e.target.value)} value={category} id="search-input" placeholder="Categories" className='focus' />
                </Form>
                <Button onClick={handleSubmit}>Find</Button>
            </Box>
            {loading ? <div>Loading...</div> : <Box display="flex" height="100%">
                <Box flex={1} mt={2} height="100%" overflow="auto">
                    <Table responsive>
                        <thead>
                            <tr>
                                <th className="w-25">SERVICE <i class="fa fa-solid fa-copy text-primary" style={{ fontSize: 18, cursor: 'pointer' }} onClick={() => copyToClipboard("qa2")}></i></th>
                                <th className="w-25">RANK IN BFF</th>
                                <th className="w-25">BFF <i class="fa fa-solid fa-copy text-primary" style={{ fontSize: 18, cursor: 'pointer' }} onClick={() => copyToClipboard("bff")}></i></th>
                                <th className="w-25">RANK IN SERVICE</th>
                                <th className="w-25">RESULTS</th>
                            </tr>
                        </thead>
                        <tbody>
                            {(response?.bff)?.map((product, i) => {
                                const serviceResponse = response?.qa2?.[i]
                                const bffResponse = response?.bff?.[i]
                                const serviceField = serviceResponse?.field
                                const bffField = bffResponse?.field
                                return <tr>
                                    <td className="w-25"><Box>{product[1]}</Box><Box><span style={{ marginRight: 20 }}>{i + 1}. </span><Typography variant='body' style={{ paddingRight: 10 }}>{serviceResponse?.[serviceField]}</Typography></Box></td>
                                    <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='body' style={{ paddingRight: 10 }}>{response?.bff?.findIndex(e => e?.[bffField] == serviceResponse?.[serviceField]) + 1}</Typography></Box></td>
                                    <td className="w-25"><Box>{product[1]}</Box><Box><span style={{ marginRight: 20 }}>{i + 1}. </span><Typography variant='body' style={{ paddingRight: 10 }}>{bffResponse?.[bffField]}</Typography></Box></td>
                                    <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='body' style={{ paddingRight: 10 }}>{response?.qa2?.findIndex(e => e?.[serviceField] == bffResponse?.[bffField]) + 1}</Typography></Box></td>
                                    <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='body' style={{ paddingRight: 10 }}>{serviceResponse?.[serviceField] == bffResponse?.[bffField] ? <font color="green">SAME</font> : <font color="red">DIFFERENT</font>}</Typography></Box></td>
                                    {/* <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='caption' style={{ paddingRight: 10 }}>{response?.bff[i]?.product_id}</Typography></Box></td> */}
                                </tr>
                            })}
                        </tbody>
                    </Table>
                </Box>
            </Box>}
        </div >
    )
}

export default Elasticsearch