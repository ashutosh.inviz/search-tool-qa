import { searchAPI } from "../../api/search";

// export const getRankingProductsInfo = async (rankingResponse) => {
//   const query = rankingResponse?.boosts.map((e) => e.product.toLowerCase()).join(" ")
//   const data = await searchAPI.getBFFResponse({ search_term: query });
//   const products = Object.fromEntries(
//     data.data.searchresult.map((e) => [e.productId, e])
//   );
//   // return rankingResponse?.boosts.map((e) => products[e.product]).filter((e) => e)
//   return products
// };

export const getRankingProductsInfo = async (rankingResponse) => {
  const query = rankingResponse?.boosts.map((e) => e.product.toLowerCase().toUpperCase()).join("|")
  const data = await searchAPI.getProductInfo(query);
  const products = Object.fromEntries(
    data.data.map((e) => [e.payload.code_string, e.payload])
  );
  // return rankingResponse?.boosts.map((e) => products[e.product]).filter((e) => e)
  return products
};
