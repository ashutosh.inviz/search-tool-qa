import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Table,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import SearchBarIcon from "../Icons/HeaderIcons/SearchBarIcon";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Notification from "../Notification/Notification";
import { spellcheckAPI } from "../../api/spellcheck";
import { qpsAPI } from "../../api/qps";
import { mappingsAPI } from "../../api/mappings";
import ReactJson from "react-json-view";
import { facetsAPI } from "../../api/facets";
import displayNames from "../../data/solrFieldDisplayName.json";
import { searchAPI } from "../../api/search";
import RankingUniversal from "../Ranking/RankingUniversal";
import { rankingAPI } from "../../api/ranking";
import brandsMapping from "../../data/brands_mapping.json";
import categoryNames from "../../data/mapping_updated.json";
import { getRankingProductsInfo } from "./util";
import RankingPreview from "../Ranking/RankingPreview";

const options = {
  autoClose: 4000,
  closeButton: false,
  hideProgressBar: true,
  position: toast.POSITION.BOTTOM_RIGHT,
};

function TabPanel(props) {
  const { children, value, index } = props;
  if (value !== index) {
    return null;
  }

  return (
    <Box
      style={{
        flex: 1,
        display: "flex",
        overflow: "auto",
        width: "100%",
        height: "100%",
        flexDirection: "column",
      }}
    >
      {children}
    </Box>
  );
}

function UniversalSearch() {
  const [searchTerm, setSearchTerm] = useState("");
  const [category, setCategory] = useState("");
  const [requestTime, setRequestTime] = useState({});
  const [tab, setTab] = useState("spellcheck");
  const [response, setResponse] = useState([]);
  const [bffTab, setBffTab] = useState("code_response");
  const [askForToken, setAskForToken] = useState(false);
  const [currentFacet, setCurrentFacet] = useState();
  const [rankingProductsInfo, setRankingProductsInfo] = useState([]);
  const copyToClipboard = () => {
    toast(<Notification message="copied to clipboard" type="info" />, options);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const spellcheckData = await spellcheckAPI.getNormalizeQuery(searchTerm);
    setResponse((prev) => ({ ...prev, spellcheck: spellcheckData.data }));
    setRequestTime((prev) => ({
      ...prev,
      spellcheck: spellcheckData.headers["request-duration"],
    }));
    const qpsData = await qpsAPI.getQpsData(
      spellcheckData.data.normalized_query
    );
    if (!searchTerm) {
      qpsData.data = { filters: [] };
    }
    setResponse((prev) => ({ ...prev, qps: qpsData.data }));
    setRequestTime((prev) => ({
      ...prev,
      qps: qpsData.headers["request-duration"],
    }));
    let categories =
      qpsData.data.filters.find((e) => e.field_name == "category_en_string_mv")
        ?.values || [];
    let brands =
      qpsData.data.filters.find((e) => e.field_name == "brand_en_string_mv")
        ?.values || [];
    if (category) {
      categories = category.startsWith("[")
        ? JSON.parse(category)
        : category.toUpperCase().split(/\s*[|,]\s*/);
    }
    const rulesInfo = await searchAPI.getRulesInfo(searchTerm.toLowerCase());
    setResponse((prev) => ({ ...prev, rules: rulesInfo.data }));

    rankingAPI
      .getServiceRanking({
        search_term: spellcheckData.data.normalized_query,
        category_id: categories.join("|"),
        brand_id: brands.join("|"),
      })
      .then((e) => {
        setResponse((prev) => ({ ...prev, ranking: e.data }));
        getRankingProductsInfo(e.data).then((resp) => {
          // console.log("INFO", resp);
          setRankingProductsInfo(resp);
        });
        setRequestTime((prev) => ({
          ...prev,
          ranking: e.headers["request-duration"],
        }));
      });

    facetsAPI
      .getServiceFacets({
        search_term: spellcheckData.data.normalized_query,
        category_id: categories.join("|"),
        brand_id: brands.join("|"),
      })
      .then((e) => {
        setResponse((prev) => ({ ...prev, dynamic_facets: e.data }));
        setRequestTime((prev) => ({
          ...prev,
          dynamic_facets: e.headers["request-duration"],
        }));
      });
    searchAPI
      .getBFFResponse({
        search_term: searchTerm,
        category_id: categories.join("|"),
      })
      .then((e) => {
        setResponse((prev) => ({ ...prev, bff: e.data }));
        setRequestTime((prev) => ({
          ...prev,
          bff: e.headers["request-duration"],
        }));
      });

    searchAPI.getCoreSearchResponse(searchTerm).then(
      (coreSearchResponse) => {
        setResponse((prev) => ({
          ...prev,
          core_search: coreSearchResponse.data,
        }));
      },
      (err) => {
        if (err.response.status == 401) {
          setAskForToken(true);
        }
      }
    );
  };

  return (
    <div style={{ height: "100%", display: "flex", flexDirection: "column" }}>
      <Box display="flex">
        <Form
          style={{ flex: 1 }}
          className="d-none d-sm-block"
          inline
          onSubmit={handleSubmit}
        >
          <Input
            style={{ width: "100%", marginRight: 10 }}
            onChange={(e) => setSearchTerm(e.target.value)}
            value={searchTerm}
            id="search-input"
            placeholder="Search term"
            className="focus"
          />
          {/* <Input style={{ flex: 1, marginRight: 10 }} onChange={e => setCategory(e.target.value)} value={category} id="search-input" placeholder="Categories" className='focus' /> */}
        </Form>
        <Form
          style={{ flex: 1 }}
          className="d-none d-sm-block"
          inline
          onSubmit={handleSubmit}
        >
          {/* <Input style={{ flex: 1, marginRight: 10 }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Search term" className='focus' /> */}
          <Input
            style={{ width: "100%", marginRight: 10 }}
            onChange={(e) => setCategory(e.target.value)}
            value={category}
            id="search-input"
            placeholder="Categories"
            className="focus"
          />
        </Form>
        <Button onClick={handleSubmit}>Find</Button>
      </Box>
      <Box flex={1} overflow="auto" height="100%">
        <Tabs
          style={{ zIndex: 1, background: "white", position: "sticky", top: 0 }}
          onChange={(e, v) => setTab(v)}
          value={tab}
        >
          <Tab value="spellcheck" label="SpellCheck" />
          <Tab value="qps" label="QPS" />
          <Tab value="dynamic_facets" label="Dynamic Facets" />
          <Tab value="core_search" label="Core Search" />
          <Tab value="bff" label="BFF" />
          <Tab value="ranking" label="Ranking" />
        </Tabs>
        <Box flex={1} overflow="auto">
          <TabPanel value={tab} index="spellcheck">
            <Typography variant="caption">{requestTime.spellcheck}</Typography>
            <Box display="flex">
              <Box flex={1}>
                <Typography variant="h6">Spellcheck</Typography>
                <ReactJson
                  quotesOnKeys={false}
                  theme="monokai"
                  displayDataTypes={false}
                  src={response.spellcheck}
                />
              </Box>
              <Box flex={1}>
                <Typography variant="h6">Rule</Typography>
                <ReactJson
                  quotesOnKeys={false}
                  theme="monokai"
                  displayDataTypes={false}
                  src={response.rules}
                />
              </Box>
            </Box>
          </TabPanel>
          <TabPanel value={tab} index="qps">
            <Box display="flex">
              <Box flex={1}>
                <Box m={2} component="h6">
                  Confident Categories
                </Box>
                <table
                  width={"100%"}
                  cellPadding={10}
                  cellSpacing={0}
                  border="1"
                >
                  {response?.qps?.confident_categories?.map((v) => {
                    return (
                      <tr>
                        <td>
                          <Typography variant="caption">{v}</Typography>
                        </td>
                        <td>
                          <Typography variant="caption">
                            {categoryNames[v]}
                          </Typography>
                        </td>
                      </tr>
                    );
                  })}
                </table>
                <Box m={2} component="h6">
                  Categories
                </Box>
                <table
                  width={"100%"}
                  cellPadding={10}
                  cellSpacing={0}
                  border="1"
                >
                  {response?.qps?.filters
                    .find((e) => e.field_name == "category_en_string_mv")
                    ?.values?.map((v) => {
                      return (
                        <tr>
                          <td>
                            <Typography variant="caption">{v}</Typography>
                          </td>
                          <td>
                            <Typography variant="caption">
                              {categoryNames[v]}
                            </Typography>
                          </td>
                        </tr>
                      );
                    })}
                </table>
                <Box m={2} component="h6">
                  Brands
                </Box>
                <table
                  width={"100%"}
                  cellPadding={10}
                  cellSpacing={0}
                  border="1"
                >
                  {response?.qps?.filters
                    .find((e) => e.field_name == "brand_en_string_mv")
                    ?.values?.map((v) => {
                      return (
                        <tr>
                          <td>
                            <Typography variant="caption">{v}</Typography>
                          </td>
                          <td>
                            <Typography variant="caption">
                              {brandsMapping[v]}
                            </Typography>
                          </td>
                        </tr>
                      );
                    })}
                </table>
              </Box>
              <ReactJson
                quotesOnKeys={false}
                collapsed={1}
                theme="monokai"
                displayDataTypes={false}
                src={response.qps}
                style={{ flex: 1 }}
              />
            </Box>
          </TabPanel>
          <TabPanel value={tab} index="dynamic_facets">
            <Typography variant="caption">
              {requestTime.dynamic_facets}
            </Typography>
            <Box flex={1} display="flex">
              <Box flex={1}>
                {response?.dynamic_facets?.facets.map((facet) => {
                  return (
                    <Accordion>
                      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Box>
                          <Box>
                            <Typography>{displayNames[facet.name]}</Typography>
                          </Box>
                          <Box>
                            <Typography variant="caption">
                              {facet.name}
                            </Typography>
                          </Box>
                        </Box>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Box display="flex" flexWrap="wrap">
                          {facet.values?.map((v) => {
                            return (
                              <Box
                                display="flex"
                                flexDirection="column"
                                style={{
                                  borderRadius: 5,
                                  background: "#F3E6E8",
                                  padding: "2px 15px",
                                  margin: 2,
                                  marginLeft: 5,
                                }}
                              >
                                <Typography variant="body2">
                                  {brandsMapping[v.name] || v.name}
                                </Typography>
                                <Typography variant="body2">
                                  {facet.name == "brand_en_string_mv" && v.name}
                                </Typography>
                                <Typography variant="caption">
                                  {v.count}
                                </Typography>
                              </Box>
                            );
                          })}
                        </Box>
                      </AccordionDetails>
                    </Accordion>
                  );
                })}
              </Box>
              <ReactJson
                quotesOnKeys={false}
                style={{ flex: 1 }}
                collapsed={2}
                theme="monokai"
                displayDataTypes={false}
                src={response.dynamic_facets}
              />
            </Box>
          </TabPanel>
          <TabPanel value={tab} index="bff">
            {/* <Typography variant="caption">{requestTime.bff}</Typography> */}
            <Tabs
              style={{
                zIndex: 1,
                background: "white",
                position: "sticky",
                top: 0,
              }}
              onChange={(e, v) => setBffTab(v)}
              value={tab}
            >
              <Tab value="preview" label="Preview" />
              <Tab value="code_response" label="Response" />
            </Tabs>
            <TabPanel value={bffTab} index="preview">
              <RankingUniversal
                rankingProductsInfo={response?.bff?.searchresult || []}
              />
            </TabPanel>
            <TabPanel value={bffTab} index="code_response">
              <ReactJson
                quotesOnKeys={false}
                collapsed={1}
                theme="monokai"
                displayDataTypes={false}
                src={response.bff}
              />
            </TabPanel>
          </TabPanel>
          <TabPanel value={tab} index="core_search">
            <ReactJson
              quotesOnKeys={false}
              theme="monokai"
              displayDataTypes={false}
              collapsed={1}
              src={response.core_search?.meta}
            />
            {askForToken && (
              <Box p={5}>
                <Input
                  onKeyDown={(e) => {
                    if (e.key == "Enter") {
                      localStorage.setItem("bt.token", e.target.value);
                      window.location.reload();
                    }
                  }}
                  placeholder="Paste the token here and hit ENTER"
                />
              </Box>
            )}
          </TabPanel>
          <TabPanel value={tab} index="ranking">
            {/* <RankingUniversal
              rankingProductsInfo={rankingProductsInfo || []}
              originalRanking={response?.ranking}
            /> */}
            <RankingPreview
              response={response || {}}
              rankingProductsInfo={rankingProductsInfo}
            />
          </TabPanel>
        </Box>
      </Box>
    </div>
  );
}

export default UniversalSearch;
