import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box, Typography } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import { mappingsAPI } from '../../api/mappings';
import { facetsAPI } from '../../api/facets';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

const heading = {
    qa2: 'Service',
    core: 'Core',
    bff: 'BFF'
}

const APIs = [
    { api: "getServiceFacets", name: 'qa2' }, { api: "getCoreFacets", name: 'core' }, { api: 'getBFFFacets', name: 'bff' }
]

function DynamicFacets() {
    const [searchTerm, setSearchTerm] = useState('')
    const [response, setResponse] = useState()
    const [category, setCategory] = useState('')
    const copyToClipboard = () => {
        navigator.clipboard.writeText("shoe")
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }

    const loadData = () => {
        const s = localStorage.getItem("searchTerm") || ''
        const c = localStorage.getItem("categories") || ''
        setSearchTerm(s)
        setCategory(c)
    }

    const handleSubmit = e => {
        localStorage.getItem("searchTerm",searchTerm)
        e.preventDefault()
        const categories = category.startsWith("[") ? JSON.parse(category) : category.toUpperCase().split(/\s*[|,]\s*/)
        APIs.forEach(a => {
            facetsAPI[a.api]({ search_term: searchTerm, category_id: categories.join("|") }).then(d => {
                console.log("GOT RESPONSE FROM ", a.name)
                setResponse(prev => ({ ...prev, [a.name]: facetsAPI.responseMapping[a.name](d) }))
            }).catch(() => {
                setResponse(prev => ({ ...prev, [a.name]: undefined }))
            })
        })
    }

    return (
        <div style={{ height: '100%' }}>
            <Form className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                <Box>
                    <Box display="flex"><Input style={{ flex: 1, marginRight: 10 }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Search term" className='focus' />
                        <Input style={{ flex: 1, marginRight: 10 }} onChange={e => setCategory(e.target.value)} value={category} id="search-input" placeholder="Categories" className='focus' />
                        <Button onClick={handleSubmit}>Find</Button>
                        <Button onClick={loadData}>Load</Button>
                    </Box>
                </Box>
            </Form>
            <Box display="flex" height="100%">
                {Object.keys(response || {})?.map(type => {
                    return <Box flex={1} mt={2} height="100%" overflow="auto">
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th className="w-25">{heading[type]}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {response[type]?.map((facet, i) => <tr>
                                    <td className="w-25"><Box>{facet[1]}</Box><Box><Typography variant='caption' style={{ paddingRight: 10 }}>{facet[0]}</Typography></Box></td>
                                </tr>)}
                            </tbody>
                        </Table>
                    </Box>
                })}
            </Box>
        </div >
    )
}

export default DynamicFacets