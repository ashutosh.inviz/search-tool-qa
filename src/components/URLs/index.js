import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import services from './services';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

function URLs() {
    const [env, setEnv] = useState("qa2")
    const [allServices, setAllServices] = useState(services[env])
    const copyToClipboard = (q) => {
        navigator.clipboard.writeText(q)
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }
    // const handleSubmit = async e => {
    //     e.preventDefault()
    //     const queries = searchTerm.split(/\s*,\s*/)
    //     const promises = queries.map(async query => spellcheckAPI.getNormalizeQuery(query).then(d => d.data))
    //     Promise.all(promises).then(responsesAll => {
    //         setResponses(responsesAll)
    //     })
    // }

    const confirmAndSet = () => {
        const pass = localStorage.getItem("prodPassword") || prompt("Enter the password")
        if (pass !== "cCE,}jY+7z<p") {
            return alert("Wrong password")
        }
        else {
            localStorage.setItem("prodPassword", pass)
        }
        setEnv("prod")
    }

    useEffect(() => {
        setAllServices(services[env])
    }, [env])

    return (
        <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
            <div className="d-flex p-2">
                <h5 style={{ textTransform: 'capitalize' }}>{env}</h5>
                <UncontrolledDropdown>
                    <DropdownToggle caret>
                        &nbsp; {env} &nbsp;
                    </DropdownToggle>
                    <DropdownMenu >
                        <DropdownItem onClick={() => setEnv("qa2")}>QA2</DropdownItem>
                        <DropdownItem onClick={() => setEnv("preprod")}>Preprod</DropdownItem>
                        <DropdownItem onClick={confirmAndSet}>Prod</DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </div>
            <div style={{ flex: 1, overflow: 'auto' }}>
                <Table>
                    {Object.keys(allServices).map(service => {
                        return <tr>
                            <td style={{ textTransform: 'capitalize' }}>{service.split("_").join(" ")}</td>
                            <td><a target='_blank' href={allServices[service].swagger || allServices[service].route || allServices[service].url}>Docs <i className="fa fa-send" /></a></td>
                            {!allServices[service].hidden && <td><Box display="flex">
                                <Box flex={1}><Input disabled value={allServices[service].url} /></Box> <a onClick={() => copyToClipboard(allServices[service].url)}><i className="eva eva-copy"></i></a></Box></td>}
                        </tr>
                    })}
                </Table>
            </div>
        </div >
    )
}

export default URLs