import { PREPROD_BFF_URL, PREPROD_BUSINESS_TOOL, PREPROD_CORE_SEARCH, PREPROD_TATA, PROD_BFF_URL, PROD_BUSINESS_TOOL, PROD_TATA, QA2_BFF_URL, QA2_BUSINESS_TOOL, QA2_CORE_SEARCH, QA2_ELASTIC, QA2_KIBANA, QA2_TATA } from "../../api/urls"

const services = {}

const qa2_base = "https://internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com"
const preprod_base = "https://internal-a4d2eba22727441aeb5d0ce46d51a387-2106664561.ap-south-1.elb.amazonaws.com"
const prod_base = "https://internal-af4d1e0d0f0cf46f5a3d390143a2c6d6-747451277.ap-south-1.elb.amazonaws.com"

const urlFactory = (base, service_name, route) => {
    return {
        url: `${base}/${service_name}`,
        route: `${base}/${service_name}/${route}`,
        get swagger() { return `${this.url}/docs` }
    }
}


services.qa2 = {
    tatacliq_UI: {
        url: QA2_TATA, swagger: QA2_TATA
    },
    bff: { route: QA2_BFF_URL, url: QA2_BFF_URL },
    dynamic_facets: urlFactory(qa2_base, "insearch-service-dynamic-facets", "search"),
    ranking: urlFactory(qa2_base, "insearch-service-ranking", "search"),
    query_parser: urlFactory(qa2_base, "insearch-service-query-parser", "parser-v3"),
    spellcheck: urlFactory(qa2_base, "insearch-service-spellcheck", "spellcheck"),
    autosuggest: urlFactory(qa2_base, "insearch-service-autosuggest", "v1/autosuggest2"),
    rules: urlFactory(qa2_base, "insearch-service-rules", "field.get"),
    indexer: urlFactory(qa2_base, "insearch-service-indexer"),
    core_search: {
        url: `${QA2_CORE_SEARCH}/search/v1/query`,
        get swagger() { return `${QA2_CORE_SEARCH}/swagger-ui/index.html` }
    },
    business_tool: {
        url: QA2_BUSINESS_TOOL,
        swagger: QA2_BUSINESS_TOOL
    },
    elasticsearch: {
        url: QA2_ELASTIC,
        hidden: true,
        swagger: QA2_KIBANA
    }
}

services.preprod = {
    tatacliq_UI: {
        url: PREPROD_TATA, swagger: PREPROD_TATA
    },
    bff: { route: PREPROD_BFF_URL, url: PREPROD_BFF_URL },
    dynamic_facets: urlFactory(preprod_base, "insearch-service-dynamic-facets", "search"),
    ranking: urlFactory(preprod_base, "insearch-service-ranking", "search"),
    query_parser: urlFactory(preprod_base, "insearch-service-query-parser", "parser-v3"),
    spellcheck: urlFactory(preprod_base, "insearch-service-spellcheck", "spellcheck"),
    autosuggest: urlFactory(preprod_base, "insearch-service-autosuggest", "v1/autosuggest2"),
    indexer: urlFactory(preprod_base, "insearch-service-indexer"),
    rules: urlFactory(preprod_base, "insearch-service-rules", "field.get"),
    core_search: {
        url: `${PREPROD_CORE_SEARCH}}/search/v1/query`,
        get swagger() { return `${PREPROD_CORE_SEARCH}/swagger-ui/index.html` }
    },
    business_tool: {
        url: PREPROD_BUSINESS_TOOL,
        swagger: PREPROD_BUSINESS_TOOL
    }
}

services.prod = {
    tatacliq_UI: {
        url: PROD_TATA, swagger: PROD_TATA
    },
    bff: { route: PROD_BFF_URL, url: PROD_BFF_URL },
    dynamic_facets: urlFactory(prod_base, "insearch-service-dynamic-facets", "search"),
    ranking: urlFactory(prod_base, "insearch-service-ranking", "search"),
    query_parser: urlFactory(prod_base, "insearch-service-query-parser", "parser-v3"),
    spellcheck: urlFactory(prod_base, "insearch-service-spellcheck", "spellcheck"),
    autosuggest: urlFactory(prod_base, "insearch-service-autosuggest", "v1/autosuggest2"),
    indexer: urlFactory(prod_base, "insearch-service-indexer"),
    rules: urlFactory(prod_base, "insearch-service-rules", "field.get"),
    core_search: {
        url: `${QA2_CORE_SEARCH}/search/v1/query`,
        get swagger() { return `${QA2_CORE_SEARCH}/swagger-ui/index.html` }
    },
    business_tool: {
        url: PROD_BUSINESS_TOOL,
        swagger: PROD_BUSINESS_TOOL,
    }
}
export default services