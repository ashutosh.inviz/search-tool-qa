import { Box } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { searchAPI } from "../../api/search";
import ProductView from "./ProductView";

function RankingUniversal({ rankingProductsInfo, originalRanking }) {
  return (
    <Box
      display="grid"
      gridTemplateColumns="repeat(auto-fit, minmax(200px, 1fr));"
      gridGap={10}
      pt={2}
    >
      {rankingProductsInfo?.map((e,i) => {
        return <ProductView item={e || {}} original={originalRanking?.boosts[i]} />;
      })}
    </Box>
  );
}

export default RankingUniversal;
