import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Table,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import SearchBarIcon from "../Icons/HeaderIcons/SearchBarIcon";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box, Divider, Typography } from "@material-ui/core";
import Notification from "../Notification/Notification";
import { spellcheckAPI } from "../../api/spellcheck";
import { mappingsAPI } from "../../api/mappings";
import { facetsAPI } from "../../api/facets";
import { rankingAPI } from "../../api/ranking";
import { qpsAPI } from "../../api/qps";
import { searchAPI } from "../../api/search";

const options = {
  autoClose: 4000,
  closeButton: false,
  hideProgressBar: true,
  position: toast.POSITION.BOTTOM_RIGHT,
};

function RankingPreview({ response: res, rankingProductsInfo }) {
  const response = {};
  response.bff = rankingAPI.responseMapping.bff({
    data: res.bff || { searchresult: [] },
  });
  response.ranking = rankingAPI.responseMapping.qa2({
    data: res.ranking || { boosts: [] },
  });
  const copyToClipboard = (env, field) => {
    console.log(response[env], field);
    const prefix = field === "color_style_code" ? "sc_" : "";
    const separator = field === "color_style_code" ? "|" : " ";
    navigator.clipboard.writeText(
      response[env]
        .map((pr) => `${prefix}${pr[field]?.toLowerCase()}`)
        .join(separator)
    );
    toast(<Notification message="copied to clipboard" type="info" />, options);
  };

  return (
    <div style={{ height: "100%", display: "flex", flexDirection: "column" }}>
      <Box flex={1} mt={2} height="100%" overflow="auto">
        <Table responsive>
          <thead>
            <tr>
              <th className="w-25">
                SERVICE{" "}
                <i
                  class="fa fa-solid fa-copy text-primary"
                  style={{ fontSize: 18, cursor: "pointer" }}
                  onClick={() =>
                    copyToClipboard("ranking", response?.ranking?.[0]?.field)
                  }
                ></i>
              </th>
              <th className="w-25">RANK IN BFF</th>
              <th className="w-25">
                BFF{" "}
                <i
                  class="fa fa-solid fa-copy text-primary"
                  style={{ fontSize: 18, cursor: "pointer" }}
                  onClick={() =>
                    copyToClipboard("bff", response?.bff?.[0]?.field)
                  }
                ></i>
              </th>
              <th className="w-25">RANK IN SERVICE</th>
              <th className="w-25">RESULTS</th>
            </tr>
          </thead>
          <tbody>
            {response?.bff?.map((product, i) => {
              const serviceResponse = response?.ranking?.[i];
              const bffResponse = response?.bff?.[i];
              const serviceField = serviceResponse?.field;
              const bffField = bffResponse?.field;
              const serviceProductInfo =
                rankingProductsInfo[serviceResponse?.product];
              return (
                <tr>
                  <td className="w-25">
                    <Box display="flex">
                      <span style={{ marginRight: 20 }}>{i + 1}. </span>
                      <Box>
                        <Typography variant="body" style={{ paddingRight: 10 }}>
                          {serviceResponse?.value}
                        </Typography>
                        <Box
                          padding="1px 5px"
                          borderRadius={5}
                          bgcolor="#F6F2E1"
                          border="1px solid #CCC39F"
                        >
                          <Typography variant="caption">
                            {serviceProductInfo?.title_text_en||'NA'}
                          </Typography>
                        </Box>
                        {serviceResponse && (
                          <Box>
                            <Typography
                              variant="caption"
                              style={{ paddingRight: 10 }}
                            >
                              {serviceResponse?.cutSize ? (
                                <font color="green">Cutsize: True</font>
                              ) : (
                                <font color="red">Cutsize: False</font>
                              )}
                            </Typography>
                          </Box>
                        )}
                      </Box>
                    </Box>
                  </td>
                  <td className="w-25">
                    <Box>
                      <Typography variant="body" style={{ paddingRight: 10 }}>
                        {response?.bff?.findIndex(
                          (e) =>
                            e?.[bffField] == serviceResponse?.value
                        ) + 1}
                      </Typography>
                    </Box>
                  </td>
                  <td className="w-25">
                    <Box display="flex">
                      <span style={{ marginRight: 20 }}>{i + 1}. </span>
                      <Box>
                        <Typography variant="body" style={{ paddingRight: 10 }}>
                          {bffResponse?.[bffField]}
                        </Typography>
                        <Box
                          padding="1px 5px"
                          borderRadius={5}
                          bgcolor="#F6F2E1"
                          border="1px solid #CCC39F"
                        >
                          <Typography variant="caption">
                            {bffResponse?.productname||'NA'}
                          </Typography>
                        </Box>
                        <Box>
                          <Typography
                            variant="caption"
                            style={{ paddingRight: 10 }}
                          >
                            {bffResponse?.cutsizeFlag ? (
                              <font color="green">Cutsize: True</font>
                            ) : (
                              <font color="red">Cutsize: False</font>
                            )}
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </td>
                  <td className="w-25">
                    <Box>
                      <Typography variant="body" style={{ paddingRight: 10 }}>
                        {response?.ranking?.findIndex(
                          (e) => e?.value == bffResponse?.[bffField]
                        ) + 1}
                      </Typography>
                    </Box>
                  </td>
                  <td className="w-25">
                    <Box>
                      <Typography variant="body" style={{ paddingRight: 10 }}>
                        {serviceResponse?.value ==
                        bffResponse?.[bffField] ? (
                          <font color="green">SAME</font>
                        ) : (
                          <font color="red">DIFFERENT</font>
                        )}
                      </Typography>
                    </Box>
                  </td>
                  {/* <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='caption' style={{ paddingRight: 10 }}>{response?.bff[i]?.product_id}</Typography></Box></td> */}
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Box>
    </div>
  );
}

export default RankingPreview;
