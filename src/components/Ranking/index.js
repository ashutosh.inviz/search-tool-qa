import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Table,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
import SearchBarIcon from "../Icons/HeaderIcons/SearchBarIcon";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box, Divider, Typography } from "@material-ui/core";
import Notification from "../Notification/Notification";
import { spellcheckAPI } from "../../api/spellcheck";
import { mappingsAPI } from "../../api/mappings";
import categoryMapping from "../../data/new_cat_mapping20may.json";
import { facetsAPI } from "../../api/facets";
import { rankingAPI } from "../../api/ranking";
import { qpsAPI } from "../../api/qps";
import { searchAPI } from "../../api/search";
import { getRankingProductsInfo } from "../UniversalSearch/util";

const options = {
  autoClose: 4000,
  closeButton: false,
  hideProgressBar: true,
  position: toast.POSITION.BOTTOM_RIGHT,
};

const heading = {
  qa2: "Service",
  core: "Core",
  bff: "BFF",
};

const APIs = [
  { api: "getServiceRanking", name: "qa2" },
  { api: "getBFFRanking", name: "bff" },
];

function Ranking() {
  const [searchTerm, setSearchTerm] = useState("");
  const [response, setResponse] = useState();
  const [category, setCategory] = useState("");
  const [brand, setBrand] = useState("");
  const [qpsCategories, setQpsCategories] = useState([]);
  const [qpsBrands, setQpsBrands] = useState([]);
  const [limit, setLimit] = useState(100);
  const [fetchLimit, setFetchLimit] = useState(2000);
  const [spellCheckResponse, setSpellCheckResponse] = useState({});
  const [loading, setLoading] = useState(false);
  const [rankingProductInfo, setRankingProductInfo] = useState();
  const copyToClipboard = (env, field) => {
    console.log(response[env], field);
    const prefix = field === "color_style_code" ? "sc_" : "";
    const separator = field === "color_style_code" ? "|" : " ";
    navigator.clipboard.writeText(
      response[env]
        .map((pr) => `${prefix}${pr[field]?.toLowerCase()}`)
        .join(separator)
    );
    toast(<Notification message="copied to clipboard" type="info" />, options);
  };

  const loadData = () => {
    const s = localStorage.getItem("searchTerm") || "";
    const c = localStorage.getItem("categories") || "";
    setSearchTerm(s);
    setCategory(c);
  };

  const handleSubmit = async (e) => {
    localStorage.getItem("searchTerm", searchTerm);
    setLoading(true);
    e.preventDefault();
    const spellcheckData = await spellcheckAPI.getNormalizeQuery(searchTerm);
    // console.log(spellcheckData)
    setSpellCheckResponse(spellcheckData.data);
    let categories = [];
    let brands = [];
    const qpsCats = await qpsAPI
      .getQpsData(spellcheckData.data.normalized_query, "v3")
      .then((e) => {
        console.log("CATEGORIES", e.data);
        // const cats =
        //   (!category &&
        //     e.data.filters?.find(
        //       (e) => e?.field_name == "category_en_string_mv"
        //     )?.values) ||
        //   [];
        const cats = (!category && e.data.confident_categories) || [];
        console.log("CATS", cats);
        const brds =
          (!brand &&
            e.data.filters?.find((e) => e?.field_name == "brand_en_string_mv")
              ?.values) ||
          [];
        categories = cats.length > 0 && searchTerm ? cats : category.split("|");
        brands = brds.length > 0 && searchTerm ? brds : brand.split("|");
        // console.log("CATEGORIES", categories, brands);
      });
    APIs.forEach((a, i) => {
      rankingAPI[a.api]({
        search_term:
          a.name == "qa2" ? spellcheckData.data.normalized_query : searchTerm,
        category_id: categories.join("|"),
        brand_id: brands.join("|"),
        limit: limit,
        fetch_limit: fetchLimit,
      })
        .then((d) => {
          console.log(
            "GOT RESPONSE FROM ",
            a.name,
            rankingAPI.responseMapping[a.name](d)
          );
          if (a.name == "qa2") {
            getRankingProductsInfo(d.data).then(setRankingProductInfo);
          }
          setResponse((prev) => ({
            ...prev,
            [a.name]: rankingAPI.responseMapping[a.name](d),
          }));
          if (i == 1) {
            setLoading(false);
          }
        })
        .catch(() => {
          setResponse((prev) => ({ ...prev, [a.name]: undefined }));
        });
    });
  };

  useEffect(() => {
    // const items = response?.qa2?.map(e=>e[e.field]).join("|") || ""
    // searchAPI.getProductInfo(items).then(console.log)
  }, [response?.qa2]);

  return (
    <div style={{ height: "100%", display: "flex", flexDirection: "column" }}>
      <Box display="flex">
        <Input
          style={{ width: 150 }}
          onChange={(e) => setLimit(e.target.value)}
          value={limit}
          id="search-input"
          placeholder="Limit (40)"
          className="focus"
        />
        <Input
          style={{ width: 150 }}
          onChange={(e) => setFetchLimit(e.target.value)}
          value={fetchLimit}
          id="search-input"
          placeholder="Fetch Limit (500)"
          className="focus"
        />
        <Form
          style={{ flex: 1 }}
          className="d-none d-sm-block"
          inline
          onSubmit={handleSubmit}
        >
          <Input
            style={{ width: "100%", marginRight: 10 }}
            onChange={(e) => setSearchTerm(e.target.value)}
            value={searchTerm}
            id="search-input"
            placeholder="Search term"
            className="focus"
          />
        </Form>
        <Form
          style={{ flex: 1 }}
          className="d-none d-sm-block"
          inline
          onSubmit={handleSubmit}
        >
          <Input
            style={{ width: "100%", marginRight: 10 }}
            onChange={(e) => setCategory(e.target.value)}
            value={category}
            id="search-input"
            placeholder="Categories"
            className="focus"
          />
        </Form>
        <Form
          style={{ flex: 1 }}
          className="d-none d-sm-block"
          inline
          onSubmit={handleSubmit}
        >
          <Input
            style={{ width: "100%", marginRight: 10 }}
            onChange={(e) => setBrand(e.target.value)}
            value={brand}
            id="search-input"
            placeholder="brand"
            className="focus"
          />
        </Form>
        <Button onClick={handleSubmit}>Find</Button>
      </Box>
      <Box display="flex">
        <span id="tooltipcontroller">
          <i className="eva eva-info"></i>QPS
        </span>
      </Box>
      <UncontrolledTooltip target="tooltipcontroller" placement="bottom">
        <Typography>QPS categories</Typography>
        {qpsCategories.map((category) => {
          return (
            <Box>
              <Typography variant="body1">{category[0]}</Typography>
              <Typography variant="caption">{category[1]}</Typography>
            </Box>
          );
        })}
      </UncontrolledTooltip>
      {response?.qa2 && (
        <Typography>
          Category: {categoryMapping[category] || category}
        </Typography>
      )}
      {response?.qa2 && (
        <Box>
          <Typography>Recall from Service: {response?.qa2?.length}</Typography>
          <Typography>Recall from BFF: {response?.bff?.length}</Typography>
        </Box>
      )}
      {spellCheckResponse?.normalized_query && (
        <Typography>
          Spell normalized: {spellCheckResponse?.normalized_query}
        </Typography>
      )}

      {loading ? (
        <div>Loading...</div>
      ) : (
        <Box display="flex" height="100%">
          <Box flex={1} mt={2} height="100%" overflow="auto">
            <Table responsive>
              <thead>
                <tr>
                  <th className="w-25">
                    SERVICE{" "}
                    <i
                      class="fa fa-solid fa-copy text-primary"
                      style={{ fontSize: 18, cursor: "pointer" }}
                      onClick={() =>
                        copyToClipboard("qa2", response?.qa2?.[0]?.field)
                      }
                    ></i>
                  </th>
                  <th className="w-25">RANK IN BFF</th>
                  <th className="w-25">
                    BFF{" "}
                    <i
                      class="fa fa-solid fa-copy text-primary"
                      style={{ fontSize: 18, cursor: "pointer" }}
                      onClick={() =>
                        copyToClipboard("bff", response?.bff?.[0]?.field)
                      }
                    ></i>
                  </th>
                  <th className="w-25">RANK IN SERVICE</th>
                  <th className="w-25">RESULTS</th>
                </tr>
              </thead>
              <tbody>
                {(response?.bff?.length > response?.qa2?.length
                  ? response?.bff
                  : response?.qa2
                )?.map((product, i) => {
                  const serviceResponse = response?.qa2?.[i];
                  const bffResponse = response?.bff?.[i];
                  const serviceField = serviceResponse?.field;
                  const bffField = bffResponse?.field;
                  const serviceProductInfo =
                    rankingProductInfo?.[serviceResponse?.product];
                  // console.log(serviceProductInfo)
                  return (
                    <tr>
                      <td className="w-25">
                        {serviceResponse && (
                          <Box display="flex">
                            <span style={{ marginRight: 20 }}>{i + 1}. </span>
                            <Box>
                              <Typography
                                variant="body"
                                style={{ paddingRight: 10 }}
                              >
                                {serviceResponse?.value}
                              </Typography>
                              <Box
                                padding="1px 5px"
                                borderRadius={5}
                                bgcolor="#F6F2E1"
                                border="1px solid #CCC39F"
                              >
                                <Typography variant="caption">
                                  {serviceProductInfo?.title_text_en || "NA"}
                                </Typography>
                              </Box>
                              <Box>
                                <Typography
                                  variant="caption"
                                  style={{ paddingRight: 10 }}
                                >
                                  {serviceResponse?.cutSize ? (
                                    <font color="green">Cutsize: True</font>
                                  ) : (
                                    <font color="red">Cutsize: False</font>
                                  )}
                                </Typography>
                              </Box>
                            </Box>
                          </Box>
                        )}
                      </td>
                      <td className="w-25">
                        <Box>
                          <Typography
                            variant="body"
                            style={{ paddingRight: 10 }}
                          >
                            {response?.bff?.findIndex(
                              (e) => e?.[bffField] == serviceResponse?.value
                            ) + 1}
                          </Typography>
                        </Box>
                      </td>
                      <td className="w-25">
                        {bffResponse && (
                          <Box display="flex">
                            <span style={{ marginRight: 20 }}>{i + 1}. </span>
                            <Box>
                              <Typography
                                variant="body"
                                style={{ paddingRight: 10 }}
                              >
                                {bffResponse?.[bffField]}
                              </Typography>
                              <Box
                                padding="1px 5px"
                                borderRadius={5}
                                bgcolor="#F6F2E1"
                                border="1px solid #CCC39F"
                              >
                                <Typography variant="caption">
                                  {bffResponse?.productname || "NA"}
                                </Typography>
                              </Box>
                              <Box>
                                <Typography
                                  variant="caption"
                                  style={{ paddingRight: 10 }}
                                >
                                  {bffResponse?.cutsizeFlag ? (
                                    <font color="green">Cutsize: True</font>
                                  ) : (
                                    <font color="red">Cutsize: False</font>
                                  )}
                                </Typography>
                              </Box>
                            </Box>
                          </Box>
                        )}
                      </td>
                      <td className="w-25">
                        <Box>
                          <Typography
                            variant="body"
                            style={{ paddingRight: 10 }}
                          >
                            {response?.qa2?.findIndex(
                              (e) => e?.value == bffResponse?.[bffField]
                            ) + 1}
                          </Typography>
                        </Box>
                      </td>
                      <td className="w-25">
                        <Box>
                          <Typography
                            variant="body"
                            style={{ paddingRight: 10 }}
                          >
                            {serviceResponse?.value ==
                            bffResponse?.[bffField] ? (
                              <font color="green">SAME</font>
                            ) : (
                              <font color="red">DIFFERENT</font>
                            )}
                          </Typography>
                        </Box>
                      </td>
                      {/* <td className="w-25"><Box>{product[1]}</Box><Box><Typography variant='caption' style={{ paddingRight: 10 }}>{response?.bff[i]?.product_id}</Typography></Box></td> */}
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Box>
        </Box>
      )}
    </div>
  );
}

export default Ranking;
