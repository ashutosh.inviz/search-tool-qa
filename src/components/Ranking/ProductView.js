import { Box, Typography } from "@material-ui/core";
import React from "react";

function ProductView({ item,original }) {
  if (!item.price) {
    return <Box>
      <Box style={{ width: 200, height: 200 }}></Box>
      <Typography>{original.color_style_code || original.product_id}</Typography>
    </Box>;
  }
  return (
    <Box>
      <Box
        style={{
          maxWidth: 200,
          position: "relative",
          borderRadius: 10,
          overflow: "hidden",
          border: "1px solid #ffffff80",
        }}
      >
        <Box
          style={{
            height: 200,
            width: 200,
          }}
        >
          <img
            style={{ height: "100%", width: "100%", objectFit: "cover" }}
            src={item?.imageURL}
          />
        </Box>
        <Box
          position="absolute"
          top={0}
          padding={"2px 5px"}
          bgcolor="#123EA0"
          color="white"
        >
          <Typography variant="caption">
            Cutsize: {JSON.stringify(item.cutsizeFlag)}
          </Typography>
        </Box>
        <Box
          style={{
            position: "absolute",
            bottom: 0,
            background: "#00000080",
            padding: 10,
            color: "white",
            width: "100%",
            minHeight: 70,
          }}
        >
          <Typography fontSize={12} variant="subtitle1">
            {item.brandname}
          </Typography>
          <Box fontSize={8}>{item.productname}</Box>
          <Box fontSize={12} bgcolor="black" color="#FBC5CA">
            {item?.price.sellingPrice.commaFormattedValue}
          </Box>
        </Box>
      </Box>
      <Typography>{item.styleCode || item.productId}</Typography>
    </Box>
  );
}

export default ProductView;
