import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';
import { mappingsAPI } from '../../api/mappings';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

function Mappings() {
    const [searchTerm, setSearchTerm] = useState('')
    const [catMappingResponse, setCatMappingResponse] = useState()
    const [facetMappingResponse, setFacetMappingResponse] = useState()
    const [facet, setFacet] = useState('')
    const copyToClipboard = () => {
        navigator.clipboard.writeText("shoe")
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }
    const handleSubmitFacet = e => {
        e.preventDefault()
        const facets = facet.split(/\s*[|,]\s*/)
        mappingsAPI.getDisplayNames(facets).then(e => setFacetMappingResponse(e.data))
        setCatMappingResponse(undefined)
    }

    const handleSubmit = e => {
        e.preventDefault()
        if (searchTerm.toUpperCase().startsWith("MSH")) {
            const categories = searchTerm.toUpperCase().split(/\s*[|,]\s*/)
            mappingsAPI.getDescriptions(categories).then(e => {
                setCatMappingResponse(e.data)
                setFacetMappingResponse(undefined)
            })
        }
        else if (searchTerm.startsWith("[")) {
            const categories = JSON.parse(searchTerm)
            mappingsAPI.getDescriptions(categories).then(e => {
                setCatMappingResponse(e.data)
                setFacetMappingResponse(undefined)
            })
        }
        else {
            mappingsAPI.getCategories(searchTerm).then(e => {
                setCatMappingResponse(e.data)
                setFacetMappingResponse(undefined)
            })
        }
    }

    useEffect(() => {
        window.getValues = mappingsAPI.getUniqueValues
    }, [])

    return (
        <div>
            <Box display="flex">
                <Box flex={1}>
                    <Form className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                        <Input style={{ width: '50%' }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Category or brand" className='focus' />
                        <Button onClick={handleSubmit}>Find</Button>
                    </Form>
                </Box>
                <Box flex={1}>
                    <Form className="d-none d-sm-block" inline onSubmit={handleSubmitFacet}>
                        <Input style={{ width: '50%' }} onChange={e => setFacet(e.target.value)} value={facet} id="search-input" placeholder="Facet or display name" className='focus' />
                        <Button onClick={handleSubmitFacet}>Find</Button>
                    </Form>
                </Box>
            </Box>
            {catMappingResponse?.length > 0 && <Box mt={2}>
                <Table responsive>
                    <thead>
                        <tr>
                            <th className="w-25">Category or Brand code</th>
                            <th className="w-25">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        {catMappingResponse?.map((category, i) => <tr>
                            <td className="w-25"><span style={{ paddingRight: 10 }}>{i + 1}.</span>{category[0]}</td>
                            <td className="w-25">{category[1]}</td>
                        </tr>)}
                    </tbody>
                </Table>
            </Box>}
            {facetMappingResponse?.length > 0 && <Box mt={2}>
                <Table responsive>
                    <thead>
                        <tr>
                            <th className="w-25">Facet</th>
                            <th className="w-25">Display Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {facetMappingResponse.map((facet, i) => <tr>
                            <td className="w-25"><span style={{ paddingRight: 10 }}>{i + 1}.</span>{facet[0]}</td>
                            <td className="w-25">{facet[1]}</td>
                        </tr>)}
                    </tbody>
                </Table>
            </Box>}
        </div >
    )
}

export default Mappings