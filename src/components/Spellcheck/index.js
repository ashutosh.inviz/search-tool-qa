import React, { useEffect, useState } from 'react'
import { Button, Form, Col, FormGroup, Input, InputGroup, InputGroupAddon, Table, Row } from 'reactstrap'
import SearchBarIcon from '../Icons/HeaderIcons/SearchBarIcon'
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box } from '@material-ui/core';
import Notification from '../Notification/Notification';
import { spellcheckAPI } from '../../api/spellcheck';

const options = {
    autoClose: 4000,
    closeButton: false,
    hideProgressBar: true,
    position: toast.POSITION.BOTTOM_RIGHT,
};

function Spellcheck() {
    const [searchTerm, setSearchTerm] = useState('')
    const [responses, setResponses] = useState([])
    const copyToClipboard = (q) => {
        navigator.clipboard.writeText(q)
        toast(<Notification message="copied to clipboard" type="info" />, options)
    }

    const handleSubmit = async e => {
        e.preventDefault()
        const queries = searchTerm.split(/\s*,\s*/)
        const promises = queries.map(async query => spellcheckAPI.getNormalizeQuery(query).then(d => d.data))
        Promise.all(promises).then(responsesAll => {
            setResponses(responsesAll)
        })
    }
    return (
        <div>
            <Form className="d-none d-sm-block" inline onSubmit={handleSubmit}>
                <Input style={{ width: '50%' }} onChange={e => setSearchTerm(e.target.value)} value={searchTerm} id="search-input" placeholder="Search queries" className='focus' />
                <Button onClick={handleSubmit}>Find</Button>
            </Form>
            {responses.length > 0 && <Box mt={2}>
                <Table responsive>
                    <thead>
                        <tr>
                            <th className="w-25">Original</th>
                            <th className="w-25">Corrected</th>
                            <th className="w-25">Normalized</th>
                        </tr>
                    </thead>
                    <tbody>
                        {responses.map(response => <tr>
                            <td className="w-25">{response.query}</td>
                            <td className="w-25">{response.corrected_query}</td>
                            <td className="w-25">{response.normalized_query} <i class="fa fa-solid fa-copy text-primary" style={{ fontSize: 18, cursor: 'pointer' }} onClick={() => copyToClipboard(response.normalized_query)}></i></td>
                        </tr>)}
                    </tbody>
                </Table>
            </Box>}
        </div >
    )
}

export default Spellcheck