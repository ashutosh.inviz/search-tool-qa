import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Table,
  Row,
} from "reactstrap";
import SearchBarIcon from "../Icons/HeaderIcons/SearchBarIcon";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Widget from "../../components/Widget/Widget.js";
import successIcon from "../../assets/notificationsIcons/successIcon.svg";
import closeIcon from "../../assets/notificationsIcons/closeIcon.svg";
import { Box, Typography } from "@material-ui/core";
import Notification from "../Notification/Notification";
import { spellcheckAPI } from "../../api/spellcheck";
import { mappingsAPI } from "../../api/mappings";
import { facetsAPI } from "../../api/facets";

const options = {
  autoClose: 4000,
  closeButton: false,
  hideProgressBar: true,
  position: toast.POSITION.BOTTOM_RIGHT,
};

const heading = {
  qa2: "Service",
  core: "Core",
  bff: "BFF",
};

const APIs = [
  { api: "getServiceFacets", name: "qa2" },
  { api: "getCoreFacets", name: "core" },
  { api: "getBFFFacets", name: "bff" },
];

function generateRegExp() {
  const columns = ["product_views", "cart_adds", "orders", "gmv"];
  const cols = columns.map((e) => String.raw`\b${e}\b`).join("|");
  const validOps = ["+", "-", "*", "/", "%", "s", "(", ")"];
  const ops = validOps.map((e) => "\\" + e).join("|");
  const extras = [String.raw`(\d+(?:\.\d+)?)`].join("|");
  const final = String.raw`^(${[ops, cols, extras].join("|")})+$`;
  return {
    columns: cols,
    operators: ops,
    final,
    extras,
  };
}

function validateExpression(expr, format = false) {
  const { final, operators, columns, extras } = generateRegExp();
  console.log(extras, new RegExp(extras, "g"));
  try {
    const result = new RegExp(final).test(expr);
    if (!result) {
      throw new SyntaxError("Token error");
    }
    try {
      Function(expr);
    } catch (e) {
      throw new SyntaxError("ParseError: " + e.message);
    }
  } catch (e) {
    return { error: true, message: e.message };
  }
  // if(format)
  expr = expr.replace(/\s+/g, "");
  expr = expr.replace(new RegExp(`(${operators})`, "g"), (e) => {
    return ` ${e} `;
  });
  let html = expr.replace(new RegExp(`(${operators})`, "g"), (e) => {
    return `<font color="white"
>${e}</font>`;
  });
  html = html.replace(new RegExp(`(${columns})`, "g"), (e) => {
    return `<font color="brown"
>${e}</font>`;
  });
  console.log(html);
  html = html.replace(new RegExp(`${extras}`, "g"), (e) => {
    console.log(e, "EXTRA");
    return `<font color="#42BDE5"
>${e}</font>`;
  });
  return { error: false, message: "success", expr, html };
}

function ComputedFields() {
  const [formula, setFormula] = useState("");

  const handleChange = (e) => {
    const ele = document.getElementById("free-flow-text");
    const res = validateExpression(e.target.innerText);
    console.log(res);
    if (res.html) {
      setFormula(res.html);
      setTimeout(()=>{
        var tag = ele;
      // Creates range object
      var setpos = document.createRange();
      // Creates object for selection
      var set = window.getSelection();
      // Set start position of range
      setpos.setStart(tag.childNodes[0], 1);
      // Collapse range within its boundary points
      // Returns boolean
      setpos.collapse(true);
      // Remove all ranges set
      set.removeAllRanges();
      // Add range with respect to range object.
      set.addRange(setpos);
      // Set cursor on focus
      tag.focus();
      },100)
    }
  };

  return (
    <div style={{ height: "100%" }}>
      <div
        id="free-flow-text"
        onKeyUp={handleChange}
        style={{ width: 400, height: 100, background: "black", padding: 10 }}
        contentEditable
        dangerouslySetInnerHTML={{ __html: formula }}
      ></div>
    </div>
  );
}

export default ComputedFields;
