// -- React and related libs
import React, { useEffect } from "react";
import { Switch, Route, Redirect } from "react-router";
import { HashRouter } from "react-router-dom";

// -- Redux
import { connect } from "react-redux";

// -- Custom Components
import LayoutComponent from "./components/Layout/Layout";
import ErrorPage from "./pages/error/ErrorPage";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";

// -- Redux Actions
import { logoutUser } from "./actions/auth";

// -- Third Party Libs
import { ToastContainer } from "react-toastify";

// -- Services
import isAuthenticated from "./services/authService";

// -- Component Styles
import "./styles/app.scss";
import axios from "axios";

const PrivateRoute = ({ dispatch, component, ...rest }) => {
  // if (!isAuthenticated(JSON.parse(localStorage.getItem("authenticated")))) {
  //   dispatch(logoutUser());
  //   return (<Redirect to="/login" />)
  // } else {
  //   return (
  //     <Route { ...rest } render={props => (React.createElement(component, props))} />
  //   );
  // }
  return (
    <Route
      {...rest}
      render={(props) => React.createElement(component, props)}
    />
  );
};

const App = (props) => {
  useEffect(() => {
    axios.interceptors.request.use(
      function (config) {
        config.headers["request-startTime"] = Date.now();
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use((response) => {
      const start = response.config.headers["request-startTime"];
      const end = Date.now();
      const milliseconds = Math.round(end - start);
      response.headers["request-duration"] = milliseconds;
      return response;
    });
  }, []);
  return (
    <div>
      <ToastContainer />
      <HashRouter>
        <Switch>
          <Route
            path="/"
            exact
            render={() => <Redirect to="/template/info" />}
          />
          <Route
            path="/template"
            exact
            render={() => <Redirect to="/template/info" />}
          />
          <PrivateRoute
            path="/template"
            dispatch={props.dispatch}
            component={LayoutComponent}
          />
          <Route path="/login" exact component={Login} />
          <Route path="/error" exact component={ErrorPage} />
          <Route path="/register" exact component={Register} />
          <Route component={ErrorPage} />
          <Route
            path="*"
            exact={true}
            render={() => <Redirect to="/error" />}
          />
        </Switch>
      </HashRouter>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(App);
