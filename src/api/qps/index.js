import axios from "axios"
import services from "../../components/URLs/services"
import { QUERY_PARSER } from "../urls"
const env = localStorage.getItem("globalENV") || "qa2"
const service = services[env]

export const qpsAPI = {}

qpsAPI.getQpsData = (q,version="v3") => {
    return axios.get(service.query_parser.route.replace("parser-v3",`parser-${version}`), { params: { prob_thresh: 0.3, query: q } })
}

