import axios from "axios"
import categoryMappings from "../../data/new_cat_mapping20may.json"
import DisplayNames from "../../data/solrFieldDisplayName.json"
import brandMappings from "../../data/brands_mapping.json"
import { BACKEND } from "../urls"
const categories = Object.entries(categoryMappings) 
const brands = Object.entries(brandMappings) 
const fields = Object.entries(DisplayNames)
export const mappingsAPI = {}

mappingsAPI.getUniqueValues = (facet) => {
    return axios.get(`${BACKEND}/mapping/${facet}`)
}

mappingsAPI.getDisplayNames = (facets) => {
    const regex = new RegExp(facets.map(e=>`(${e})`).join("|"), "ig")
    facets = fields.filter(e=>`${e[1]}_${e[0]}`.match(regex)).map(e=>e[0])
    console.log(regex,facets)
    return Promise.resolve({ data: facets.map(e=>[e,DisplayNames[e]]) })
}

mappingsAPI.getFacetNames = (displayName) => {
    const regex = new RegExp(displayName, "ig")
    return Promise.resolve({ data: fields.filter(e=>e[1].match(regex)) })
}

mappingsAPI.getCategories = (description) => {
    const regex = new RegExp(description, "ig")
    return Promise.resolve({ data: [...categories,...brands].filter(e => e.join("_").match(regex)) })
}

mappingsAPI.getDescriptions = (categories) => {
    return Promise.resolve({ data: categories.map(e => [e,categoryMappings[e]]) })
}
