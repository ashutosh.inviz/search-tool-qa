import axios from "axios"
import services from "../../components/URLs/services"
import { SPELLCHECK_URL } from "../urls"

const env = localStorage.getItem("globalENV") || "qa2"
const service = services[env]

export const spellcheckAPI = {}

spellcheckAPI.getNormalizeQuery = (query) => {
    return axios.get(service.spellcheck.route, { params: { query } })
}