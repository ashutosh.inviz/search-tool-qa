import axios from "axios"
import services from "../../components/URLs/services"
import DisplayNames from "../../data/solrFieldDisplayName.json"
import { BACKEND, QA2_BFF_URL, FACETS_QA2 } from "../urls"
const env = localStorage.getItem("globalENV") || "qa2"
export const facetsAPI = {}
const service = services[env]

facetsAPI.getServiceFacets = (params) => {
    return axios.get(service.dynamic_facets.route, { params })
}

facetsAPI.getCoreFacets = (params) => {
    return axios.get(`${BACKEND}/${env}/search`, { params })
}

facetsAPI.getBFFFacets = (params) => {
    const bffsearchText = `${params.search_term}:relevance:inStockFlag:true${!params.search_term ? ":"+params.category_id.split(/\s*[|,]\s*/).filter(e => e).map(e => `category:${e}`).join(":"):""}`
    return axios.get(service.bff.route, { params: { searchText: bffsearchText } })
}

facetsAPI.getFacetsFromAllSources = async (params) => {
    const s = await facetsAPI.getServiceFacets(params)
    const core = await facetsAPI.getCoreFacets(params)
    const bff = await facetsAPI.getBFFFacets(params)

    return Promise.resolve({
        data: {
            qa2: s.data.facets.map(e => [e.name, DisplayNames[e.name]]),
            core: core.data.facets.map(e => [e.label, e.displayName]),
            bff: bff.data.facetdata.map(e => [e.key, e.name])
        }
    })
}

facetsAPI.responseMapping = {
    qa2: (d) => d.data.facets.map(e => [e.name, DisplayNames[e.name]]),
    core: (d) => d.data.facets?.map(e => [e.label, e.displayName]),
    bff: (d) => d.data.facetdata?.map(e => [e.key, e.name])
}