import axios from "axios"
import services from "../../components/URLs/services"
import DisplayNames from "../../data/solrFieldDisplayName.json"
import { BACKEND, QA2_BFF_URL, FACETS_QA2 } from "../urls"
const env = localStorage.getItem("globalENV") || "qa2"
export const elasticAPI = {}
const service = services[env]

elasticAPI.getProductDescriptions = (index, productIds) => {
    const route = `${service.elasticsearch.url}/${index}/_search`
    const body =
    {
        "query": {
            "bool": {
                "should": [
                    { "ids": { "type": "_doc", "values": productIds } }
                ]
            }
        }
    }
    return axios.get(route, { data: body })
}

elasticAPI.getProductURL = (index, productId) => {
    const route = `${service.elasticsearch.url}/${index}/_doc/${productId}`
    return route
}