export const BACKEND = "http://localhost:8124"

export const SPELLCHECK_URL = 'https://internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com/insearch-service-spellcheck/spellcheck'

export const FACETS_QA2 = "https://internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com/insearch-service-dynamic-facets/search"

export const QA2_BFF_URL = "https://searchbffqa2.tataunistore.com/products/mpl/search/?isKeywordRedirect=true&isKeywordRedirectEnabled=true&channel=WEB&isMDE=true&isTextSearch=false&isFilter=false&qc=false&page=0&isSuggested=false&isPwa=true&typeID=all"

export const PREPROD_BFF_URL2 = "https://searchbffpreprod.tataunistore.com/products/mpl/search/?isKeywordRedirect=true&isKeywordRedirectEnabled=true&channel=WEB&isMDE=true&isTextSearch=false&isFilter=false&qc=false&page=0&isSuggested=false&isPwa=true&typeID=all"

export const PREPROD_BFF_URL = "https://searchbffpreprod.tataunistore.com/products/mpl/search/?isTextSearch=false&isFilter=false&isPwa=false&channel=mobile&typeID=all&isMDE=true&isKeywordRedirect=false&isKeywordRedirectEnabled=true&visualFilter=&providePopular=true&searchCategory=all&isSuggested=false"

export const PROD_BFF_URL = "https://searchbff.tatacliq.com/products/mpl/search/?isKeywordRedirect=true&isKeywordRedirectEnabled=true&channel=WEB&isMDE=true&isTextSearch=false&isFilter=false&qc=false&page=0&isSuggested=false&isPwa=true&typeID=all"

export const QUERY_PARSER = "https://internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com/insearch-service-query-parser/parser-v3"

export const QA2_CORE_SEARCH = "http://internal-k8s-applicat-insearch-11a03ef7da-208009536.ap-south-1.elb.amazonaws.com"

export const PREPROD_CORE_SEARCH = "http://internal-k8s-applicat-insearch-243f1fbff8-32310217.ap-south-1.elb.amazonaws.com"

// export const QA2_BUSINESS_TOOL = "http://internal-k8s-applicat-insearch-7aa7c6825c-1764606227.ap-south-1.elb.amazonaws.com"
export const QA2_BUSINESS_TOOL = "https://searchbusinesstoolqa.tataunistore.com/"

export const PREPROD_BUSINESS_TOOL = "http://internal-k8s-applicat-insearch-b93cca3439-1445998171.ap-south-1.elb.amazonaws.com/"

export const PROD_BUSINESS_TOOL = "http://internal-k8s-applicat-insearch-b9674c8ed6-818407482.ap-south-1.elb.amazonaws.com/"

export const QA2_TATA = "http://qa2.tataunistore.com"
export const PREPROD_TATA = "http://preprod4.tataunistore.com"
export const PROD_TATA = "http://tatacliq.com"

export const BT_BACKEND_QA2 = "https://internal-k8s-applicat-insearch-7dfa4c6129-1930508644.ap-south-1.elb.amazonaws.com/"

export const QA2_ELASTIC = "http://elastic:52a1Mz1KHX292sAD1iItsO44@internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com:80"
export const QA2_KIBANA = "https://internal-af2d45fb9429746509b1067d3567ff77-437995855.ap-south-1.elb.amazonaws.com/kibana/app/dev_tools#/console"