import axios from "axios";
import services from "../../components/URLs/services";
import DisplayNames from "../../data/solrFieldDisplayName.json";
import { BACKEND, QA2_BFF_URL, FACETS_QA2 } from "../urls";
const env = localStorage.getItem("globalENV") || "qa2";
const testFlag = localStorage.getItem("test-flag") || "";

export const rankingAPI = {};
const service = services[env];

rankingAPI.getServiceRanking = (params) => {
  console.log(params);
  let filters = [];
  const categories = params.category_id?.split("|")?.filter(e=>e) || [];
  const brands = params.brand_id?.split("|")?.filter(e=>e) || [];
  console.log("CAT",categories,brands)
  if (categories.length > 0) {
    filters.push(`category_en_string_mv:${categories.join(",")}`);
  }
  if (brands.length > 0) {
    filters.push(`brand_en_string_mv:${brands.join(",")}`);
  }
  return axios.get(service.ranking.route, {
    params: { ...params, test: testFlag, filters:filters.join("|") },
  });
};

rankingAPI.getBFFRanking = (params) => {
  //   console.log("PARAMS", params);
  const category_ids =
    !params.search_term && !params.brand_id && params.category_id
      ? ":" +
        params.category_id
          .split(/\s*[|,]\s*/)
          .filter((e) => e)
          .map((e) => `category:${e}`)
          .join(":")
      : "";
  const brand_ids =
    !params.search_term && params.brand_id
      ? ":" +
        params.brand_id
          .split(/\s*[|,]\s*/)
          .filter((e) => e)
          .map((e) => `brand:${e}`)
          .join(":")
      : "";
  const bffsearchText = `${params.search_term}:relevance:inStockFlag:true${category_ids}${brand_ids}`;
  return axios.get(service.bff.route, {
    params: {
      searchText: bffsearchText,
      pageSize: params.limit || 100,
      test: testFlag,
    },
  });
};

rankingAPI.responseMapping = {
  qa2: (d) =>
    d.data.boosts.map((e) => {
      return {
        ...e,
        field: e.product_id ? "product_id" : "color_style_code",
      };
    }),
  bff: (d) =>
    d.data.searchresult?.map((e, i) => ({
      ...e,
      field: e.styleCode ? "color_style_code" : "product_id",
      color_style_code: e.styleCode,
      product_id: e.productId,
      rank: i + 1,
    })),
};
