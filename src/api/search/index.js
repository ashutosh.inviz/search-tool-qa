import axios from "axios";
import services from "../../components/URLs/services";
import { QUERY_PARSER } from "../urls";
const env = localStorage.getItem("globalENV") || "qa2";
const service = services[env];

export const searchAPI = {};

searchAPI.getBFFResponse = (params) => {
  const testFlag = localStorage.getItem("test-flag") || ''
  const bffsearchText = `${params.search_term}:relevance:inStockFlag:true${
    !params.search_term
      ? ":" +
        params.category_id
          ?.split(/\s*[|,]\s*/)
          .filter((e) => e)
          .map((e) => `category:${e}`)
          .join(":")
      : ""
  }`;
  return axios.get(service.bff.route, {
    params: { searchText: bffsearchText, test:testFlag },
  });
};

searchAPI.getProductInfo = async (query) => {
  query = query.split(/\|/);
  const field = query[0].toLowerCase().startsWith("mp")
    ? "_id"
    : "payload.color_style_code_string_mv";
  const url = `${services.qa2.dynamic_facets.url}/productinfo`;
  const q = { [field]: { $in: query } };
  const data = await axios.post(url, { query: q, projection: {} });
  return data;
};

searchAPI.getCoreSearchResponse = (query) => {
  const url = `${services.qa2.rules.url}/core.search`;
  const token = localStorage.getItem("bt.token");
  const params = { query, limit: 1, env };
  return axios.get(url, {
    params,
    headers: {
      Authorization: "Bearer " + token,
    },
  });
};

searchAPI.getRulesInfo = (query) => {
  const url = `${service.rules.url}/rule.get`;
  const token = localStorage.getItem("bt.token");
  const params = {
    query,
    tenant: "delta2",
    app: "marketpalce",
    index: "product_index",
  };
  return axios.get(url, {
    params,
    headers: {
      Authorization: "Bearer " + token,
    },
  });
};
