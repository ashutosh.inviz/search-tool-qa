FROM node:12.22.1

EXPOSE 3000 

WORKDIR /app

COPY . .

ENV SERVER_ENV=$SERVER_ENV

RUN yarn install

CMD ["yarn", "run", "deploy"]
